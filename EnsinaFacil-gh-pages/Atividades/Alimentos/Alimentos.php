<!DOCTYPE html>
<html>
<title>Ensina Fácil</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Karma">
<style>
body,h1,h2,h3,h4,h5,h6 {font-family: "Karma", sans-serif}
.w3-bar-block .w3-bar-item {padding:20px}
</style>
<body>

  <?php
    GLOBAL $titulo, $diretorio;
    $titulo = 'Alimentos';
    $diretorio = "../../";
    include '../../Geral/cabecalho.php';
  ?>
  
<!-- !PAGE CONTENT! -->
<div class="w3-main w3-content w3-padding" style="max-width:900px;margin-top:75px">

  <!-- First Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center" id="food">
    <div class="w3-quarter">
      <a href="Alim1.php"><img src="../../w3images/Mango-Strawberry-Daiquiris-6.jpg" alt="juice" style="width:100%">
      <h3>Mix strawberry and mango, and make a good juice.</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="Alim2.php"><img src="../../w3images/watermelon-815072_1280.jpg" alt="Watermelon" style="width:100%">
      <h3>Watermelon is one of the lowest fruits in calories.</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="Alim3.php"><img src="../../w3images/buy-navel-oranges-online-020819b.jpg" alt="Cherries" style="width:100%">
      <h3>Oranges is a popular citrus fruits.</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="../../w3images/restaurant-1168609_1280 (1).jpg" alt="Pasta and Wine" style="width:100%">
      <h3>I don`t like snack bars.</h3></a>
    </div>
  </div>
  
  <!-- Second Photo Grid-->
  <div class="w3-row-padding w3-padding-16 w3-center">
    <div class="w3-quarter">
      <a href="#"><img src="../../w3images/noodles-3201631_1280 (1).jpg" alt="Popsicle" style="width:100%">
      <h3>I had instant noodles.</h3></a>
    </div>
    <div class="w3-quarter">
      <a href="#"><img src="../../w3images/coffe-1354786_1280 (1).jpg" alt="Salmon" style="width:100%">
      <h3>Would you like a cup of coffe?</h3>
    </div>
  </div>

  <!-- Pagination -->
  <div class="w3-center w3-padding-32">
    <div class="w3-bar">
      <a href="#" class="w3-bar-item w3-button w3-hover-black">&laquo;</a>
      <a href="#" class="w3-bar-item w3-black w3-button">1</a>
      <a href="#" class="w3-bar-item w3-button w3-hover-black">&raquo;</a>
    </div>
  </div>
  
</div>

</body>
</html>